
package ar.uba.fi.tdd.exercise;

//public int DEGRADATION = 1;

class GildedRose {
  Item[] items;


    public GildedRose(Item[] _items)
    {
        items = _items;
    }

    // update the quality of the emements
    public void updateQuality()
    {
      int DEGRADATION = 1;
      for (int i = 0; i < items.length; i++)
        {
            //Aged Brie,Backstage, sulfuras

            //degradacion de item
            if (!items[i].Name.equals("sulfuras"))
            {
                if (items[i].sellIn < 0)
                {
                  if (!items[i].Name.equals("Aged Brie"))
                  {
                    items[i].quality-= DEGRADATION * 2 ;
                  }
                  else
                  {
                    items[i].quality+= DEGRADATION * 3;
                  }
                }
                else if ( items[i].sellIn <= 10 && items[i].sellIn > 5)
                {
                  items[i].quality+= DEGRADATION * 2;
                }
                else if ( items[i].sellIn <= 5 && items[i].sellIn > 0 )
                {
                  items[i].quality+= DEGRADATION * 3;
                }
            }


            // la calidad no puede ser mayor a 50 o menor a cero

            if (!items[i].Name.equals("Sulfuras"))
            {
                if (items[i].quality<0)
                {
                    items[i].quality=0;
                }
                else if (items[i].quality>50)
                {
                    items[i].quality=50;
                }

            }

            //cada iteracion representa un dia sellIn menos
            items[i].sellIn-=1;

          }
        }

}
