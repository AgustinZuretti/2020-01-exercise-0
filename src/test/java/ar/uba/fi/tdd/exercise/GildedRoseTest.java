package ar.uba.fi.tdd.exercise;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class GildedRoseTest {

	@Test
	public void foo() {
			Item[] items = new Item[] { new Item("fixme", 0, 1) };
			GildedRose app = new GildedRose(items);
			app.updateQuality();
			//test1(app.items);

			assertThat("fixme").isEqualTo(app.items[0].Name);
			//System.out.println(app.items[0].quality);
			//assertThat(1).isEqualTo(app.items[0].quality);
			}
	//public void test1(Item[] items){
	//	assertThat("fixme").isEqualTo(items[0].Name);
	//	assertThat(0).isEqualTo(items[0].quality);
	}
